Drupal.behaviors.nyumc_adobe_dtm = function (context, settings) {
    if (Drupal.jsEnabled) {
        $(document).ready(function() {
            window.nyu_med_data = {};
            window.nyu_med_data.global = {};
            window.nyu_med_data.global.brand = 'nyu_langone';
            window.nyu_med_data.global.business_unit = 'education_research';
            window.nyu_med_data.global.experience = Drupal.settings.adobe_dtm_settings.experience;
            window.nyu_med_data.global.experience_section = Drupal.settings.adobe_dtm_settings.experience_section;
            window.nyu_med_data.global.page_type = '';
            window.nyu_med_data.global.page_id = '';

        });
    }
}