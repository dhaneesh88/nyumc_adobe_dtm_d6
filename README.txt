
Module: NYUMC Adobe DTM
Author: Dhaneesh Devasia

Description
===========
Adds the Adobe DTM Analytics tracking system to your website.

Requirements
============

* Adobe DTM Analytics user account

Installation
============
Copy the 'nyumc_adobe_dtm' module directory in to your Drupal
sites/all/modules directory as usual.

Usage
=====
In the settings page enter your Adobe DTM analytics URL. Also user can update
som params like Experience, Experience section. Otherwise default values will be
taken automatically

The Specs and instructions are mentioned in wiki pages:
https://nyulangone.atlassian.net/wiki/spaces/UR/pages/364708236/DTM+Libraries
https://nyulangone.atlassian.net/wiki/spaces/UR/pages/408977470/Spec+Medical+Site

All pages will now have the required JavaScript added to the
HTML can confirm this by viewing the page source from
your browser.

These defaults are changeable by the website administrator or any other
user with 'administer site configuration' permission.
