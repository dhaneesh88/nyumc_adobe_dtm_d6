<?php

/**
 * @file
 * Administrative page callbacks for the googleanalytics module.
 */

/**
 * Implementation of hook_admin_settings() for module settings configuration.
 */
function nyumc_adobe_dtm_admin_settings_form(&$form_state) {

  $form['adobe_dtm_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Adobe DTM URL'),
    '#default_value' => variable_get('adobe_dtm_url', ''),
    '#size' => 100,
  );

  $form['adobe_dtm_param_experience'] = array(
    '#type' => 'textfield',
    '#title' => t('Adobe DTM Tag Experience'),
    '#default_value' => variable_get('adobe_dtm_param_experience', 'nyu_langone_med_web'),
    '#size' => 30,
  );

  $form['adobe_dtm_param_experience_section'] = array(
    '#type' => 'textfield',
    '#title' => t('Adobe DTM Adobe DTM Tag Experience Section'),
    '#default_value' => variable_get('adobe_dtm_param_experience_section', ''),
    '#size' => 30,
  );

  return system_settings_form($form);
}